﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using Photon.Pun.UtilityScripts;
public class userSpawnManager : MonoBehaviourPunCallbacks
{
    public GameObject VrRig;
    GameObject user;
    Player UserPlayerComp;
    VideoController videoController;

    private void Start()
    {
        Vector3 spawnPos = Vector3.zero;

        object num;
        int pNum = 3;
        if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(PlayerNumbering.RoomPlayerIndexedProp, out num))
        {
            Debug.Log("Player Number = "+ num.ToString());
            int.TryParse(num.ToString(), out pNum);
        }
        pNum = PhotonNetwork.LocalPlayer.ActorNumber;
        spawnPos = FindObjectOfType<SpawnPoints>().getspawnPoint(pNum).position;

        user = PhotonNetwork.Instantiate("NetworkUser", spawnPos, Quaternion.identity);
        videoController = FindObjectOfType<VideoController>();
        VrRig.transform.position = spawnPos;
        user.GetComponent<AvatarController>().SetPlayerColor(pNum);
        videoController.SetPlayerColor(pNum);
        
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        UserPlayerComp = newPlayer;
        
        //user = PhotonNetwork.Instantiate("NetworkUser", Vector3.zero, Quaternion.identity);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        base.OnPlayerLeftRoom(otherPlayer);
        PhotonNetwork.Destroy(user);
    }
}
