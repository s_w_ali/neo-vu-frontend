﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    public GameObject FollowTarget;
    public GameObject LookTarget;

    public bool Lock = true;

    public float Speed = 1;

    public Vector3 LookAxis = new Vector3(0, 1, 0);

    public GameObject LockButton;
    public GameObject UnLockButton;

    void Start()
    {
        transform.LookAt(LookTarget.transform, LookAxis);
    }
    bool move = false;
    Vector3 targetPosition = Vector3.zero;
    // Update is called once per frame
    void LateUpdate()
    {
        if (Lock)
        {
            targetPosition = getTergerPosition();
            if (Vector3.Distance(transform.position, targetPosition) > 1)
            {
                move = true;
            }
            if (move)
            {
                transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * Speed);
                transform.LookAt(LookTarget.transform, LookAxis);
                if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
                {
                    move = false;
                }
            }
        }
    }

    public void UnlockFloatingControls(bool state)
    {
        Lock = state;
        LockButton.SetActive(Lock);
        UnLockButton.SetActive(!Lock);
    }


    Vector3 getTergerPosition()
    {
        return new Vector3(FollowTarget.transform.position.x, transform.position.y, FollowTarget.transform.position.z);
    }
    
}
