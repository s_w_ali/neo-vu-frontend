﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class AuthenticationManager : MonoBehaviour
{

    //UI
    public TMP_InputField UserNameUI;
    public TMP_InputField PasswordUI;

    string User;
    string Pass;
    void Start()
    {
        
    }

    public void SignIn()
    {
        User = UserNameUI.text;
        Pass = PasswordUI.text;

        if (Authenticate(User, Pass))
        {
            SceneManager.LoadScene(2);
        }

    }

    private bool Authenticate(string user, string pass)
    {
        return true;
    }

}
