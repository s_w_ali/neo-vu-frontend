﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using TMPro;

public class ParticipantEntery : MonoBehaviour
{
    public TextMeshProUGUI NameText;
    public GameObject HandRaisedButton;
    Player thisPlayer;
    bool isMute;
    string UserId { 
        get 
        {
            if (thisPlayer.CustomProperties[Manager.PlayerUidString] != null) 
                return thisPlayer.CustomProperties[Manager.PlayerUidString].ToString();
            Debug.Log(thisPlayer.CustomProperties[Manager.PlayerUidString]);
            return "";
        } 
    }

    VideoController videoController { get { return FindObjectOfType<VideoController>(); } }


    public void Initialize(Player player)
    {
        NameText.text = player.NickName;
        thisPlayer = player;
        videoController.AddParticipantEnteryToList(this , UserId);

        HandRaisedButton.SetActive(false);

    }
    public void RemovePlayerEntery()
    {
        Destroy(gameObject);
    }
    public void MuteButtonPressed()
    {
        isMute = !isMute;
        SetChatAudio(isMute);
    }
    void SetChatAudio(bool state)
    {
        isMute = state;
        AudioSource ChatAudio = videoController.VoiceChatAudioSource.Where(x => x.Key != null && x.Key == UserId).FirstOrDefault().Value;
        if (ChatAudio != null)
            ChatAudio.mute = state;
    }
    public void HideAvatarButtonPressed()
    {
        videoController.ToggleUserAvatarVisibility(UserId);
    }

    public void HandRaisedButtonPressed()
    {
        videoController.PresenterAllowQuestion(UserId);
        HandRaisedButton.SetActive(false);
        SetChatAudio(true);
    }

    public void HandRaised()
    {
        HandRaisedButton.SetActive(true);
    }
}
