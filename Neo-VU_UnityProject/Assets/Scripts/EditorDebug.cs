﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EditorDebug : MonoBehaviour
{
    public GraphicRaycaster graphicRaycaster;
    public OVRRaycaster VRRaycaster;
    public RectTransform MainCanvas;
    private void Awake()
    {
        if (Application.isEditor)
        {
            if(MainCanvas != null)
                MainCanvas.position = new Vector3(MainCanvas.position.x, MainCanvas.position.y, 0.5f);
            graphicRaycaster.enabled = true;
            VRRaycaster.enabled = false;

            FindObjectOfType<OVRInputModule>().enabled = false;
            FindObjectOfType<StandaloneInputModule>().enabled = true;
        }

    }
}
