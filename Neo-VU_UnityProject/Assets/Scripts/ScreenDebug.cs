﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScreenDebug : MonoBehaviour
{
    static public TextMeshProUGUI debugText;

    private void Awake()
    {
        ScreenDebug.debugText = gameObject.GetComponent<TextMeshProUGUI>();
    }

    static public void LogToScreen(string log)
    {
        if(debugText.enabled)
            debugText.text = log + "\n" + debugText.text;
    }
    static public void ClearScreen()
    {
        if (debugText.enabled)
            debugText.text = "";
    }
}
