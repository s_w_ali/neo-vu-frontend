﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Manager
{
    public const string PlayerColorString = "PlayerColor";
    public const string PlayerUidString = "PlayerUserID";
    public const string PresenterRoleString = "presenter";
    public const string SelectedVideoPackString = "SelectedVideoPack";
    public const string SelectedSessiontring = "SelectedSession";
    public static Manager Instance
    {
        get 
        {
            if (_instance == null)
                _instance = new Manager();

            return _instance; 
        }
    }
    static Manager _instance;
    public Member member;
    public Session SelectedSession;
    public System.Version VersionString = new System.Version(0, 0, 22);

    public Dictionary<int, GameObject> playerListEntries;

    public string VideoSaveFolder 
    { 
        get 
        {
            return Path.GetFullPath(Path.Combine(Application.isEditor ? Path.Combine(Application.dataPath, @"..\..\"):Application.persistentDataPath, MediaBrowser.VideoFolderName));
        } 
    }

    public string GetVersionString()
    {
        return string.Format("V {0}", VersionString.ToString());
    }

    static public string GetUserID(Photon.Realtime.Player player)
    {
        return player.CustomProperties[Manager.PlayerUidString].ToString();
    }
    static public bool IsUserPresenter(Photon.Realtime.Player player , Session session)
    {
        return session.Members.Where(x => x.Uid == GetUserID(player)).FirstOrDefault().Role == PresenterRoleString;
    }
}
