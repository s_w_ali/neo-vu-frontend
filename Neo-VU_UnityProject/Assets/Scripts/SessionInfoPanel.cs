﻿using Photon.Pun;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SessionInfoPanel : MonoBehaviour
{
    public Text SessionNameText;
    public GameObject MediaListEntryPrefab;
    public Transform MediaListParent;
    MediaBrowser mediaBrowser { get { return FindObjectOfType<MediaBrowser>(); } }
    public void Initialize(Session session)
    {
        SessionNameText.text = session.Name;
        string PackFileUid = (string)PhotonNetwork.CurrentRoom.CustomProperties["SelectedVideoPack"];
        MediaPack pack = mediaBrowser.videoPacks.Where(x => x.Uid == PackFileUid).FirstOrDefault();
        foreach (Transform item in MediaListParent.transform)
        {
            Destroy(item.gameObject);
        }
        foreach (Content content in session.Content)
        {
            if (content.IsVideoFile)
            {
                GameObject mediaListEntry = Instantiate(MediaListEntryPrefab, MediaListParent);
                mediaListEntry.GetComponent<MediaListEntery>().Initialize(content, content.Uid == pack.Uid);
            }
        }
    }
}
