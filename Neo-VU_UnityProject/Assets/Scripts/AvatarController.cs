﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using Photon.Voice.PUN;
using Photon.Pun.UtilityScripts;

public class AvatarController : MonoBehaviour
{
    public Transform Head;
    Transform HeadTarget;
    
    public Transform LeftHand;
    Transform LeftHandTarget;

    public Transform RightHand;
    Transform RightHandTarget;

    public Image SpeakerLogo;
    public Image SpeechLogo;

    public TextMeshProUGUI UserName;

    public LineRenderer lineRenderer;

    public AudioSource PlayerVoiceAudioSource;

    public GameObject [] ColorMeshes;

    public GameObject CursorGO;

    public CanvasGroup[] canvasGroups;

    MediaBrowser mediaBrowser;
    PhotonView photonView;
    VideoController videoController;
    private PhotonVoiceView photonVoiceView;

    bool isVisible = true;

    string userID { get { return photonView.Owner.CustomProperties[Manager.PlayerUidString].ToString(); } }


    private void Awake()
    {
        Spotter spotter= FindObjectOfType<Spotter>();
        HeadTarget = spotter.Head;
        LeftHandTarget = spotter.LeftHand;
        RightHandTarget = spotter.RightHand;
    }

    // Start is called before the first frame update
    void Start()
    {
        photonView = GetComponent<PhotonView>();
        photonVoiceView = GetComponent<PhotonVoiceView>();

        videoController = FindObjectOfType<VideoController>();
        if (videoController.VoiceChatAudioSource != null)
        {
            if (videoController.VoiceChatAudioSource.ContainsKey(userID))
                videoController.VoiceChatAudioSource[userID] = PlayerVoiceAudioSource;
            else
                videoController.VoiceChatAudioSource?.Add(userID, PlayerVoiceAudioSource);
        }
        object num;
        int pNum = 3;
        UserName.text = photonView.Owner.NickName;
        if (photonView.Owner.CustomProperties.TryGetValue(PlayerNumbering.RoomPlayerIndexedProp, out num))
        {
            int.TryParse(num.ToString(), out pNum);
        }
        pNum = photonView.Owner.ActorNumber;
        SetPlayerColor(pNum);
        if (photonView.IsMine)
        {
            //Head.gameObject.SetActive(false);
            LeftHand.gameObject.SetActive(false);
            RightHand.gameObject.SetActive(false);
        }

        videoController.AddToPlayerGOList(gameObject, photonView.Owner.CustomProperties[Manager.PlayerUidString].ToString());

        Debug.Log("[UserList] User EnterRoom");

    }

    // Update is called once per frame
    void Update()
    {
        //this.recorderSprite.enabled = this.photonVoiceView.IsRecording;
        //this.speakerSprite.enabled = this.photonVoiceView.IsSpeaking;
        if (photonView.IsMine && isVisible)
        {
            syncTransforms(Head,HeadTarget);
            syncTransforms(LeftHand, LeftHandTarget);
            syncTransforms(RightHand, RightHandTarget);
        }
            //this.SpeakerLogo.enabled = photonVoiceView.IsSpeaker;
            this.SpeechLogo.enabled = photonVoiceView.IsSpeaking;
    }
    void syncTransforms(Transform surce, Transform target)
    {
        surce.localPosition = target.localPosition;
        surce.rotation = target.rotation;
    }
    void syncRotation(Transform surce, Transform target)
    {
        surce.rotation = target.rotation;
    }

    public void SetPlayerColor(int playerNumber)
    {
        UserName.text += playerNumber.ToString();
        mediaBrowser = FindObjectOfType<MediaBrowser>();
        if (mediaBrowser != null)
        {
            for (int i = 0; i < ColorMeshes.Length; i++)
            {
                ColorMeshes[i].GetComponent<Renderer>().material.color = mediaBrowser.GetColor(playerNumber);
            }
            lineRenderer.endColor = mediaBrowser.GetColor(playerNumber);
            lineRenderer.startColor = mediaBrowser.GetColor(playerNumber);
            CursorGO.GetComponent<Renderer>().material.color = mediaBrowser.GetColor(playerNumber);
        }
    }

    public void ToggleVisibility()
    {
        isVisible = !isVisible;
        foreach (MeshRenderer renderer in gameObject.GetComponentsInChildren<MeshRenderer>(true))
        {
            renderer.enabled = isVisible;
        }
        foreach (SkinnedMeshRenderer skinnedMeshRenderer in gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true))
        {
            skinnedMeshRenderer.enabled = isVisible;
        }
        foreach (CanvasGroup canvasGroup in canvasGroups)
        {
            canvasGroup.alpha = isVisible ? 1 : 0;
        }
        lineRenderer.enabled = isVisible;
    }

}
