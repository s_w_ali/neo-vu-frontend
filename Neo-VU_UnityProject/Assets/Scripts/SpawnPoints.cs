﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoints : MonoBehaviour
{
    public Transform[] SpawnPointsList;
    public Transform getspawnPoint(int Index)
    {
        if (Index < 0 || Index >= SpawnPointsList.Length)
            Index = 0;
        return SpawnPointsList[Index];
    }
}
