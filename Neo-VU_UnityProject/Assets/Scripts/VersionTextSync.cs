﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionTextSync : MonoBehaviour
{
    void Start()
    {
        if(GetComponent<TMPro.TextMeshProUGUI>() != null)
            GetComponent<TMPro.TextMeshProUGUI>().text = Manager.Instance.GetVersionString();
    }

}
