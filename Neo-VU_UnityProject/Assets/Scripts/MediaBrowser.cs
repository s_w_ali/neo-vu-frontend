﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Networking;

[System.Serializable]
public class MediaPack
{
    public string Name;
    public string Uid;
    public VideoClip videoClip;
    public string FilePath;
    public VideoSource videoSource;
    public Vector2 Res;
    public RenderTexture RT;
    public Sprite Thumb;
    public Content MediaContent;
    public float DownloadProgress = 0;
    public bool Downloaded;
    public bool IsvideoFile;
    private bool downloadInProcess;
    private List<System.Action<float>> ProgressCallBackList;
    private List<System.Action> OnDoneCallBackList;
    WebComBase webComBase;
    public MediaPack()
    {
        ProgressCallBackList = new List<System.Action<float>>();
        OnDoneCallBackList = new List<System.Action>();
    }

    

    public void Initialize(Content content, WebComBase webComBase,System.Action<float> progressCallback = null, System.Action OnDone = null)
    {
        MediaContent = content;
        this.webComBase = webComBase;
        if (OnDone != null)
        OnDoneCallBackList.Add(OnDone);
        if (!Downloaded)
        {
            if(progressCallback != null)
                ProgressCallBackList.Add(progressCallback);
            if (!downloadInProcess)
            {
                downloadInProcess = true;
                this.webComBase.DownloadFile(content, Progress: DownloadProgressCallBack,
                            OnSucsess: () =>
                            {
                                File.Move(FilePath + ".download",FilePath);
                                Downloaded = true;
                                downloadInProcess = false;
                                DownloadCompleteCallBack();
                            },
                            OnError: (string errorString) =>
                            {
                                // download failed need to inform user.
                            }
                            );
            }
        }
        else 
        {
            //DownloadCompleteCallBack();
        }
    }

    public void RegisterForProgressUpdate(System.Action<float> progressCallback = null)
    {
        if (progressCallback != null)
            ProgressCallBackList.Add(progressCallback);
        progressCallback?.Invoke(DownloadProgress);
    }

    void DownloadProgressCallBack(float p)
    {
        DownloadProgress = p;
        foreach (System.Action<float> cb in ProgressCallBackList)
        {
            cb?.Invoke(DownloadProgress);
        }
    }
    void DownloadCompleteCallBack()
    {
        this.webComBase.StartCoroutine(CheckIfIconPack());

        foreach (System.Action cb in OnDoneCallBackList)
        {
            cb?.Invoke();
        }
    }

    IEnumerator CheckIfIconPack()
    {
        if (Path.GetExtension(FilePath) == ".jpg")
        {
            IsvideoFile = false;
            string imageFilePath = FilePath;
            if (File.Exists(imageFilePath))
            {
                yield return this.webComBase.LoadTexture(imageFilePath, this);
            }
        }
    }

    public override bool Equals(object obj)
    {
        return Name == ((MediaPack)obj).Name;
    }
}

public class MediaBrowser : MonoBehaviour
{
    public const string VideoFolderName = "Videos";
    string VideoFolderPath = "Videos";

    public GameObject VideoButtonPrefab;
    public List<MediaPack> videoPacks;
    private Transform ButtonsParant;
    List<Button> buttons;
    public Color[] AvatarColors;
    
    Session SelectedSession { get { return Manager.Instance.SelectedSession; } }
    WebComBase _webComBase;
    public WebComBase webComBase { get 
        {
            if (_webComBase == null)
                _webComBase = FindObjectOfType<WebComBase>();
            return _webComBase; 
        }
    }
    private void Start()
    {
        if (FindObjectsOfType<MediaBrowser>().Length == 1)
            DontDestroyOnLoad(this);
        else
            Destroy(gameObject);
        if(Application.isEditor)
            VideoFolderPath = Path.GetFullPath(Path.Combine(Application.dataPath, @"..\..\", VideoFolderName));
         else
            VideoFolderPath = Path.GetFullPath(Path.Combine(Application.persistentDataPath , VideoFolderName));

        UpdateMediaPackListFromDisk();

    }

    public void UpdateMediaPackListFromDisk()
    {
        StartCoroutine(LoadVideosFromDisk());
    }

    IEnumerator LoadVideosFromDisk()
    {
        ScreenDebug.ClearScreen();
        if (videoPacks != null)
            videoPacks.Clear();
        videoPacks = new List<MediaPack>();
        if (!Directory.Exists(VideoFolderPath))
                Directory.CreateDirectory(VideoFolderPath);
        yield return new WaitForEndOfFrame();
            foreach (string OldPendingDownloads in Directory.GetFiles(VideoFolderPath, "*.download", SearchOption.AllDirectories))
            {
                try
                {
                    File.Delete(OldPendingDownloads);
                }
                catch (System.Exception)
                {
                    ScreenDebug.LogToScreen("Error :: Unable to delete the .download file.");
                }
            }
            foreach (string mediaFile in Directory.GetFiles(VideoFolderPath))
            {
            if (Path.GetExtension(mediaFile) == ".download")
            {
                continue;
            }
            yield return new WaitForEndOfFrame();
                MediaPack pack = new MediaPack();
                string [] nameParts = Path.GetFileNameWithoutExtension(mediaFile).Split("&"[0]);
                pack.Name = nameParts[1]; 
                pack.Uid = nameParts[0];
                pack.FilePath = mediaFile;
                pack.videoSource = VideoSource.Url;
                pack.Downloaded = true;
                pack.IsvideoFile = true;
                if (Path.GetExtension(pack.FilePath) == ".jpg")
                {
                    pack.IsvideoFile = false;
                    string imageFilePath = pack.FilePath;
                    if (File.Exists(imageFilePath))
                    {

                        webComBase.LoadTextureAsync(imageFilePath, pack);
                    }
                }
                videoPacks.Add(pack);
                ScreenDebug.LogToScreen(Path.GetFileName(pack.FilePath));
            }
        
    }
    public void DownloadSesstionMedia(int sessionIndex)
    {
        Session session = Manager.Instance.member.Sessions[sessionIndex];
        Manager.Instance.member.Sessions[sessionIndex].sessionDownloadManager.OnMediaDownload +=
                () =>
                {
                    MediaSyncComplete(Manager.Instance.member,session);
                    if (Manager.Instance.member.Sessions.Count > sessionIndex + 1)
                    {
                        DownloadSesstionMedia(sessionIndex + 1);
                    }
                };
        StartCoroutine(session.sessionDownloadManager.startSessionMediaSync(session));
        //StartCoroutine(startMediaDownload());
    }
    void MediaSyncComplete(Member member, Session session)
    {
        webComBase.ContentSynced(member: member, session: session);
    }
    IEnumerator startMediaDownload()
    {
        Session ps = null;
        foreach (Session s in Manager.Instance.member.Sessions)
        {
            yield return s.sessionDownloadManager.startSessionMediaSync(s);
            //if (ps == null)
            //else
            ps.sessionDownloadManager.OnMediaDownload +=
                () =>
                {
                    s.sessionDownloadManager.startSessionMediaSync(s);
                };
            //ps = s;
        }
    }

    public void OnBrowseropen()
    {
        ClearMediaButtonList();
        buttons = new List<Button>();
        StartCoroutine(SetupVideoButtons());
    }

    int numberOfDownloadRequired = 0;
    IEnumerator SetupVideoButtons()
    {
        numberOfDownloadRequired = 0;
        ClearMediaButtonList();
        //yield return StartCoroutine(LoadVideosFromDisk());
        ButtonsParant = FindObjectOfType<Spotter>().BrowserParent;
        foreach (Content item in SelectedSession.Content)
        {
            if (item.IsVideoFile)
            {
                MediaPack vp;

                if (videoPacks.Any(x => x.Uid == item.Uid))
                {
                    vp = videoPacks.Where(x => x.Uid == item.Uid).FirstOrDefault();
                }
                else
                {
                    vp = new MediaPack();
                    vp.Name = Path.GetFileNameWithoutExtension(item.Name);
                    vp.videoSource = VideoSource.Url;
                    vp.Uid = item.Uid;
                    vp.FilePath = Path.Combine(VideoFolderPath, vp.Uid + "&" + item.Name);
                    videoPacks.Add(vp);
                }

                GameObject packButton = InstantiateButton(vp);
                vp.RegisterForProgressUpdate(packButton.GetComponent<VideoLoadingProgress>().PregressCallback);
                //vp.Initialize(item, webComBase, packButton.GetComponent<VideoLoadingProgress>().PregressCallback);
                yield return new WaitForEndOfFrame();
            }
            
        }
        //StartCoroutine(ActivateCreatButton());
        //foreach (VideoPack VideoFile in videoPacks)
        //{
        //    InstantiateButton(VideoFile);
        //    yield return new WaitForEndOfFrame();
        //}
        //foreach (var item in buttons)
        //{
        //    item.onClick.AddListener(ButtonClicked);
        //}
    }
    public void BackButton()
    {
        ClearMediaButtonList();
    }
    void ClearMediaButtonList()
    {
        if (buttons != null)
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                Destroy(buttons[i].gameObject);
            }
            buttons.Clear();
        }
    }
    public MediaPack SetUpVideoPack(Content item , System.Action<float , int> p = null, System.Action onDone = null)
    {
        MediaPack vp;
        if (videoPacks.Any(x => x.Uid == item.Uid))
        {
            vp = videoPacks.Where(x => x.Uid == item.Uid).FirstOrDefault();
        }
        else
        {
            vp = new MediaPack();
            vp.Name = Path.GetFileNameWithoutExtension(item.Name);
            vp.videoSource = VideoSource.Url;
            vp.Uid = item.Uid;
            vp.FilePath = Path.Combine(VideoFolderPath, vp.Uid+"&"+item.Name);
            videoPacks.Add(vp);
        }
        vp.Initialize(item, webComBase, (float val)=> { p.Invoke(val, videoPacks.IndexOf(vp)); }, onDone);
        return vp;
    }
    GameObject InstantiateButton(MediaPack VideoFile)
    {
        GameObject newButton = Instantiate(VideoButtonPrefab, ButtonsParant);
        newButton.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = VideoFile.Name;
        MediaPack imagePack = videoPacks.Where(x => x.FilePath.ToUpper().Contains((VideoFile.Name + ".jpg").ToUpper())).FirstOrDefault();
        if(imagePack != null)
             newButton.transform.Find("Image").GetComponent<Image>().sprite = imagePack.Thumb;
        newButton.GetComponent<VideoButtonInfo>().MyPack = VideoFile;
        buttons.Add(newButton.GetComponent<Button>());
        return newButton;
    }

    IEnumerator ActivateCreatButton()
    {
        while (numberOfDownloadRequired > 0)
        {
            yield return new WaitForEndOfFrame();
        }
    }

    public void ButtonClicked(MediaPack pack)
    {
        LobbyMenuController lobby = FindObjectOfType<LobbyMenuController>();
        lobby.SelectedVideoPack = pack;
        lobby.OnCreateRoomButtonClicked();
        foreach (var item in buttons)
        {
            item.interactable = false;
        }
        ClearMediaButtonList();
    }

    public Color GetColor(int colorChoice)
    {
        if(colorChoice <AvatarColors.Length)
            return AvatarColors[colorChoice];
        return Color.black;
    }
}
