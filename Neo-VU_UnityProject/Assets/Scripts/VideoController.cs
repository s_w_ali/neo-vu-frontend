﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using TMPro;
using Photon.Pun;
using Photon.Voice.PUN;
using System;
using System.Linq;
using System.IO;
using UnityEngine.EventSystems;

public class VideoController : MonoBehaviourPunCallbacks, IPunObservable
{
    enum PlayState { Play, Paused, Stop }

    bool OnlyMasterCanControll = false;

    public Sprite Play_Sprite;
    public Sprite Pause_Sprite;

    public Image PlayPauseImage;

    public VideoPlayer videoPlayer;

    public Slider SeekingSlider;
    public Slider VideoVolumeSlider;
    public Slider ChatVolumeSlider;

    public AudioSource VideoVolumeAudioSource;
    Dictionary<string, AudioSource> _voiceChatAudioSource;
    public Dictionary<string, AudioSource> VoiceChatAudioSource { set { _voiceChatAudioSource = value; } 
        get {
            if (_voiceChatAudioSource == null)
                _voiceChatAudioSource = new Dictionary<string, AudioSource>();
            return _voiceChatAudioSource; 
        }
    }

    public Material VideoMaterialFor3D;
    public Material VideoMaterialFor2D;

    public RenderTexture RenderTextureFor3D;
    public RenderTexture RenderTextureFor2D;

    public TextMeshProUGUI PlayTimeText;
    public TextMeshProUGUI SessionTimeText;
    public TextMeshProUGUI ParticipantCountText;
    public GameObject ParticipantHandRaisedGO;

    public TextMeshProUGUI FileNameText;
    public TextMeshProUGUI DebugText;

    public CanvasGroup MainCanvasGroup;
    public CanvasGroup[] VideoControlCanvasGroup;

    public CanvasGroup SettingsCanvasGroup;
    public CanvasGroup ParticipantCanvasGroup;

    public LineRenderer lineRenderer;

    public GameObject[] ColorMeshes;

    public GameObject RaiseHandButton;

    private PlayState state = PlayState.Play;
    private MediaBrowser mediaBrowser;



    bool seeking = false;
    bool Visible = true;

    float seekPause = 1;
    float seekPauseTime = 1;
    float sessionStartTime = 0;
    float Alpha = 1;
    float speed = 1f;

    int totalPlaytime = 0;

    private void Start()
    {
        PlayPauseImage.sprite = Pause_Sprite;

        mediaBrowser = FindObjectOfType<MediaBrowser>();
        UsersList = new Dictionary<string, GameObject>();
        SetupPlayer();
    }

    void SetupPlayer()
    {
        String PackFileUid = "";
        string fileName = "";

        MediaPack pack = new MediaPack();
        if (PhotonNetwork.CurrentRoom != null)
        {
            PackFileUid = (string)PhotonNetwork.CurrentRoom.CustomProperties[Manager.SelectedVideoPackString];
            pack = mediaBrowser.videoPacks.Where(x => x.Uid == PackFileUid).FirstOrDefault();
        }
        else
        {
            pack.Name = "Bank";
            fileName = "";
        }
        videoPlayer.url = pack.FilePath;
        videoPlayer.Prepare();
        if (pack.Name.StartsWith("3D"))
        {
            if (VideoMaterialFor3D != null)
            {
                RenderSettings.skybox = VideoMaterialFor3D;
                videoPlayer.targetTexture = RenderTextureFor3D;
            }
        }
        else
        {
            if (VideoMaterialFor2D != null)
            {
                RenderSettings.skybox = VideoMaterialFor2D;
                videoPlayer.targetTexture = RenderTextureFor2D;
            }
        }
        ParticipantCountText.text = PhotonNetwork.PlayerList.Length.ToString();
        FileNameText.text = fileName;
        videoPlayer.prepareCompleted += VideoReadyToBePlayed;
        Hide(5);
    }

    void VideoReadyToBePlayed(VideoPlayer vp)
    {
        SeekingSlider.maxValue = videoPlayer.frameCount;
        totalPlaytime = Mathf.RoundToInt(videoPlayer.frameCount / videoPlayer.frameRate);
        if (!PhotonNetwork.IsMasterClient)
        {
            EnableControlles(false);
        }
        else
        {
            RaiseHandButton.SetActive(false);
        }
        vp.Play();
    }

    private void Update()
    {
        if (state == PlayState.Play && !seeking)
        {
            SeekingSlider.SetValueWithoutNotify(videoPlayer.frame);
        }
        if (seeking)
        {
            seekPause -= Time.deltaTime;
            if (seekPause < 0)
                seeking = false;
        }

        if (OVRInput.GetUp(OVRInput.Button.Two) || Input.GetKeyUp(KeyCode.Space))
        {
            if (Visible)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }

        sessionStartTime += Time.deltaTime;
        PlayTimeText.text = TimeSpan.FromSeconds(GetPlayTime()) + "/" + TimeSpan.FromSeconds(totalPlaytime);
        SessionTimeText.text = TimeSpan.FromSeconds((int)sessionStartTime).ToString();
    }
    public static bool IsPointerOverUIElement()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        return results.Count > 0;
    }
    int GetPlayTime()
    {
        if (videoPlayer.frameRate != 0)
            return Mathf.RoundToInt(videoPlayer.frame / videoPlayer.frameRate);
        else
            return 0;
    }
    public void seek()
    {
        Debug.Log("seek");
        seeking = true;
        seekPause = seekPauseTime;
        videoPlayer.frame = (long)(SeekingSlider.value);
    }
    public void setVideoVolume()
    {
        VideoVolumeAudioSource.volume = VideoVolumeSlider.value;
    }
    public void setChatVolume()
    {
        foreach (var item in VoiceChatAudioSource.Values)
        {
            if (item != null)
                item.volume = ChatVolumeSlider.value;
        }
    }
    public void PlayPause()
    {
        if (state == PlayState.Play)
        {
            Pause();
        }
        else
        {
            Play();
        }

    }
    void Play()
    {
        PlayPauseImage.sprite = Pause_Sprite;
        videoPlayer.Play();
        state = PlayState.Play;
    }
    void Pause()
    {
        PlayPauseImage.sprite = Play_Sprite;
        videoPlayer.Pause();
        state = PlayState.Paused;
    }
    public void Stop()
    {
        Pause();
        videoPlayer.frame = 1;
        videoPlayer.Stop();
        state = PlayState.Stop;
        SeekingSlider.SetValueWithoutNotify(videoPlayer.frame);
    }
    public void Menu()
    {
        PhotonNetwork.LeaveRoom();
    }
    public override void OnLeftRoom()
    {
        PhotonNetwork.LocalPlayer.CustomProperties.Clear();
        PhotonNetwork.LoadLevel("Signin");
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(this.videoPlayer.frame);
            stream.SendNext(this.state);
        }
        else
        {
            //if (this.m_firstTake)
            //{
            //    this.m_firstTake = false;
            //}
            PlayState PrevState = this.state;
            //this.videoPlayer.frame = (long)stream.ReceiveNext();
            long frame = (long)stream.ReceiveNext();
            this.state = (PlayState)stream.ReceiveNext();

            if (PrevState != this.state)
            {
                StateUpdated();
                this.videoPlayer.frame = frame;
            }
        }
    }

    bool CanControle()
    {
        return PhotonNetwork.IsMasterClient || !OnlyMasterCanControll;
    }

    private void StateUpdated()
    {
        switch (this.state)
        {
            case PlayState.Play:
                Play();
                break;
            case PlayState.Paused:
                Pause();
                break;
            case PlayState.Stop:
                Stop();
                break;
            default:
                break;
        }
    }

    void Hide(float delay = 0)
    {
        StartCoroutine(HideRoutine(delay));
    }
    IEnumerator HideRoutine(float delay)
    {
        yield return new WaitForSeconds(delay);

        while (Alpha > 0)
        {
            Alpha -= Time.deltaTime * speed;
            MainCanvasGroup.alpha = Alpha;
            yield return new WaitForEndOfFrame();
        }
        Alpha = 0;
        Visible = false;
        MainCanvasGroup.alpha = Alpha;
        MainCanvasGroup.interactable = false;
        MainCanvasGroup.blocksRaycasts = false;
    }
    void Show()
    {
        StartCoroutine(ShowRoutine());
    }
    IEnumerator ShowRoutine()
    {
        while (Alpha < 1)
        {
            Alpha += Time.deltaTime * speed;
            MainCanvasGroup.alpha = Alpha;
            yield return new WaitForEndOfFrame();
        }
        Alpha = 1;
        Visible = true;
        MainCanvasGroup.alpha = Alpha;
        MainCanvasGroup.interactable = true;
        MainCanvasGroup.blocksRaycasts = true;
    }

    void EnableControlles(bool state)
    {
        foreach (CanvasGroup group in VideoControlCanvasGroup)
        {
            group.interactable = state;
        }
        SeekingSlider.interactable = state;
        RaiseHandButton.SetActive(true);
    }

    public void SetPlayerColor(int playerNumber)
    {
        DebugText.text += playerNumber.ToString();
        mediaBrowser = FindObjectOfType<MediaBrowser>();
        if (mediaBrowser != null)
        {
            for (int i = 0; i < ColorMeshes.Length; i++)
            {
                ColorMeshes[i].GetComponent<Renderer>().material.color = mediaBrowser.GetColor(playerNumber);
            }
            lineRenderer.endColor = mediaBrowser.GetColor(playerNumber);
            lineRenderer.startColor = mediaBrowser.GetColor(playerNumber);
        }
    }
    CanvasGroup ActiveMenu;
    public void ShowSettings()
    {
        StartCoroutine(ToggleCanvasGroupVisibilityRoutine(SettingsCanvasGroup));
    }

    public void ShowHideParticipantList()
    {
        StartCoroutine(ToggleCanvasGroupVisibilityRoutine(ParticipantCanvasGroup));
    }
    float groupAlphaVal = 1;
    float transitionSpeed = 2;
    bool BusyTCGV = false;
    IEnumerator ToggleCanvasGroupVisibilityRoutine(CanvasGroup canvasGroup)
    {
        if (!BusyTCGV)
        {
            groupAlphaVal = canvasGroup.alpha;
            transitionSpeed = 4;
            BusyTCGV = true;
            if (ActiveMenu != null && ActiveMenu != canvasGroup)
            {
                yield return HideCanvasGroupVisibilityRoutine(ActiveMenu);
                ActiveMenu = null;
            }
            if (!canvasGroup.interactable)
            {
                yield return ShowCanvasGroupVisibilityRoutine(canvasGroup);
                ActiveMenu = canvasGroup;
            }
            else
            {
                yield return HideCanvasGroupVisibilityRoutine(canvasGroup);
                ActiveMenu = null;
            }
            BusyTCGV = false;
        }
    }
    IEnumerator ShowCanvasGroupVisibilityRoutine(CanvasGroup canvasGroup)
    {
        groupAlphaVal = 0;
        canvasGroup.alpha = groupAlphaVal;

        while (groupAlphaVal < 1)
        {
            groupAlphaVal += Time.deltaTime * transitionSpeed;
            canvasGroup.alpha = groupAlphaVal;
            yield return new WaitForEndOfFrame();
        }
        groupAlphaVal = 1;
        canvasGroup.alpha = groupAlphaVal;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }
    IEnumerator HideCanvasGroupVisibilityRoutine(CanvasGroup canvasGroup)
    {
        groupAlphaVal = 1;
        canvasGroup.alpha = groupAlphaVal;

        while (groupAlphaVal > 0)
        {
            groupAlphaVal -= Time.deltaTime * transitionSpeed;
            canvasGroup.alpha = groupAlphaVal;
            yield return new WaitForEndOfFrame();
        }
        groupAlphaVal = 0;
        canvasGroup.alpha = groupAlphaVal;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }
        Dictionary<string, GameObject> UsersList;
    Dictionary<string, ParticipantEntery> UIParticmantEnteryList;
    public void AddToPlayerGOList(GameObject user,string ID)
    {
        if (UsersList == null)
            UsersList = new Dictionary<string, GameObject>();
        if (UsersList.ContainsKey(ID))
            UsersList[ID] = user;
        else
            UsersList.Add(ID, user);
        Debug.Log("[UserList] [+] User added to the list UID =" + ID);

    }

    public void RemoveToPlayerGOList(string ID)
    {
        if (UsersList == null)
            UsersList = new Dictionary<string, GameObject>();

        UsersList.Remove(ID);
        Debug.Log("[UserList] [-] User added to the list UID =" + ID);
        RemoveParticipantEnteryFromList(ID);
    }

    internal void AddParticipantEnteryToList(ParticipantEntery participantEntery, string ID)
    {
        if (ID == null)
            return;
        if (UIParticmantEnteryList == null)
            UIParticmantEnteryList = new Dictionary<string, ParticipantEntery>();

        UIParticmantEnteryList.Add(ID, participantEntery);

    }

    internal void RemoveParticipantEnteryFromList(string ID)
    {
        if (UIParticmantEnteryList.ContainsKey(ID))
        {
            UIParticmantEnteryList[ID].RemovePlayerEntery();
            UIParticmantEnteryList.Remove(ID);
        }
    }

    public void ToggleUserAvatarVisibility(string ID)
    {
        if(UsersList.ContainsKey(ID))
            UsersList[ID].GetComponent<AvatarController>().ToggleVisibility();
    }

    int HandRaisedCount = 0;
    public void RaiseHand() // this is for the Viewers when they want to raise hand
    {
        photonView.RPC("RaiseHandRPC", RpcTarget.MasterClient, Manager.Instance.member.Uid);
    }
    [PunRPC]
    void RaiseHandRPC(String ID) // this is for the master to ping when any viewer has a question.
    {
        if (Manager.IsUserPresenter(photonView.Owner, Manager.Instance.SelectedSession))
        {
            if(UIParticmantEnteryList.ContainsKey(ID))
                UIParticmantEnteryList[ID].HandRaised();
            HandRaisedCount += 1;

            ParticipantHandRaisedGO?.SetActive(true);

        }
    }
    public void PresenterAllowQuestion(string ID)
    {
        if(HandRaisedCount>0)
            HandRaisedCount -= 1;
        if(HandRaisedCount == 0)
            ParticipantHandRaisedGO?.SetActive(false);
    }

    public void UpdateParticipantCount()
    {
        ParticipantCountText.text = PhotonNetwork.PlayerList.Length.ToString();
    }
}
