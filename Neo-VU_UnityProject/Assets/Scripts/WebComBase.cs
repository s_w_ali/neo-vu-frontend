﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
public struct WebComReq
{
    public string url;
    public System.Action<string, System.Action<object>> OnSucsess;
    public System.Action<string> OnFail;
    public System.Action<object> ResponceObject;
    public byte[] PayLoad;
}
public struct WebComDataReq
{
    public Content ContentData;
    public System.Action OnSucsess;
    public System.Action<string> OnFail;
    public System.Action<float> Progress;
}

public class WebComBase : MonoBehaviour
{
    public const string BaseUrl = @"https://us-central1-neovu-vr.cloudfunctions.net/";
    public const string SessionUrl = @"sessions/";
    public const string MemberUrl = @"members/";
    public const string ContentSyncedUrl = @"content_synced";
    // Start is called before the first frame update

    List<Coroutine> DownloadRequestCoroutines;
    List<UnityWebRequest> DownloadWebRequests;

    void Start()
    {
        DownloadRequestCoroutines = new List<Coroutine>();
        DownloadWebRequests = new List<UnityWebRequest>();
        //LoadUrl(new WebComReq { url = BaseUrl + SessionUrl+ "2421" });
        //GetMemberinfo("6259",
        //    (object responceObject)=> 
        //    {
        //        if (responceObject == null)
        //            return;

        //        Members member = (Members)responceObject;
        //        Debug.Log(member);
        //        foreach (var item in member.SessionCodes)
        //        {
        //            GetSessioninfo(item,
        //                (object responceSessionObject) =>
        //                {
        //                    if (responceSessionObject == null)
        //                        return;

        //                    Session session = (Session)responceSessionObject;
        //                    Debug.Log(session);
        //                }
        //                );
        //        }
        //    }
        //    );

        //DownloadVideoFile(new Content
        //{
        //    Name = "360° Video Resuscitation of a COVID-19 Patient w Respiratory Failure Best Practices Demonstration.mp4",
        //    Url = @"https://firebasestorage.googleapis.com/v0/b/neovu-vr.appspot.com/o/lQEqT7ESuDXlDoF8C5NJfiLNKcw2%2F360%C2%B0%20Video%20Resuscitation%20of%20a%20COVID-19%20Patient%20w%20Respiratory%20Failure%20Best%20Practices%20Demonstration.mp4?alt=media&token=3fa213ed-8076-432d-a8e0-99b891da69fd"
        //}
        //, Progress: (float progress) => { Debug.Log(progress * 1.0f); });
    }

    public void GetSessioninfo(string code, System.Action<object> ResponceObject = null)
    {
        WebComReq req = new WebComReq();
        req.url = BaseUrl + SessionUrl + code;
        req.OnSucsess = DeSerializeSession;
        req.ResponceObject = ResponceObject;
        LoadUrl(req);
    }

    void DeSerializeSession(string sessionJson, System.Action<object> resObj = null)
    {
        //JsonSessionData session = JsonConvert.DeserializeObject<JsonSessionData>(sessionJson);
        //resObj.Invoke(session.session);
    }

    public void GetMemberinfo(string code, System.Action<object> ResponceObject = null, System.Action<string> OnError = null)
    {
        WebComReq req = new WebComReq();
        req.url = BaseUrl + MemberUrl + code;
        req.OnSucsess = DeSerializeMember;
        req.ResponceObject = ResponceObject;
        req.OnFail = OnError;
        req.PayLoad = null;

        LoadUrl(req);
    }
    void DeSerializeMember(string memberJson, System.Action<object> resObj = null)
    {
        Root member = JsonConvert.DeserializeObject<Root>(memberJson);
        member.Member.Sessions.RemoveAll(x => x == null);
        resObj.Invoke(member.Member);
    }
    public void StopAllCurrentDownloads()
    {
        foreach (Coroutine coroutine in DownloadRequestCoroutines)
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
            }
        }
    }
    public void AbortDownload()
    {
        if (webRequest != null)
        {
            webRequest?.Abort();
            (webRequest.downloadHandler as CustomWebRequest)?.CloseFileStream();
        }
    }
    public void DownloadFile(Content content, System.Action OnSucsess = null, System.Action<float> Progress = null, System.Action<string> OnError = null)
    {
        WebComDataReq req = new WebComDataReq();
        req.ContentData = content;
        req.OnSucsess = OnSucsess;
        req.Progress = Progress;
        req.OnFail = OnError;
        DownloadRequestCoroutines.Add(StartCoroutine(DownloadFile(req)));
    }

    public void ContentSynced(Member member, Session session)
    {
        string data = "{\"member_code\":" + member.MemberCode +
            ",\"session_uid\":\"" + session.Uid + "\"" +
            ",\"content_synced\":true}";

        byte[] payload = System.Text.Encoding.ASCII.GetBytes(data);

        WebComReq req = new WebComReq();

        req.url = BaseUrl + ContentSyncedUrl;
        req.PayLoad = payload;

        LoadUrl(req);

    }

    // Update is called once per frame
    void LoadUrl(WebComReq req)
    {
        StartCoroutine(LoadUrlRoutine(req));
    }

    static IEnumerator LoadUrlRoutine(WebComReq req)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(req.url))
        {
            // Request and wait for the desired page.
            //Debug.Log("WebComBase:: Request URL = " + req.url);
            webRequest.method = "GET";

            if (req.PayLoad != null)
            {
                webRequest.method = "POST";

                UploadHandler uploader = new UploadHandlerRaw(req.PayLoad);

                // Sends header: "Content-Type: custom/content-type";
                // Sends header: "content-type application/json";
                uploader.contentType = "application/json";

                webRequest.uploadHandler = uploader;
            }

            yield return webRequest.SendWebRequest();

            if (!string.IsNullOrEmpty(webRequest.error))
            {
                Debug.Log("WebComBase:: Request Error" + webRequest.error);
                req.OnFail?.Invoke(webRequest.error);
            }
            else
            {
                //Debug.Log("WebComBase:: Request Data" + webRequest.downloadHandler.text);
                req.OnSucsess?.Invoke(webRequest.downloadHandler.text, req.ResponceObject);
            }
        }
    }
    UnityWebRequest webRequest;
    //Pre-allocate memory so that this is not done each time data is received
    byte[] bytes = new byte[2000];

    IEnumerator DownloadFile(WebComDataReq req)
    {
        webRequest = new UnityWebRequest(req.ContentData.Url);
        webRequest.downloadHandler = new CustomWebRequest(bytes, req);
        DownloadWebRequests.Add(webRequest);
        webRequest.SendWebRequest();
        yield return webRequest;
    }

    public void LoadTextureAsync(string url, MediaPack pack)
    {
        StartCoroutine(LoadTexture(url, pack));
    }

    public IEnumerator LoadTexture(string url, MediaPack pack)
    {
        Texture2D texture = new Texture2D(128, 128);
        using (UnityWebRequest loader = UnityWebRequestTexture.GetTexture("file://" + url))
        {

            yield return loader.SendWebRequest();

            if (string.IsNullOrEmpty(loader.error))
            {
                texture = DownloadHandlerTexture.GetContent(loader);
            }
            else
            {
                Debug.Log(string.Format("Error loading Texture '{0}': {1}", loader.uri, loader.error));
            }
        }
        //WWW image = new WWW(url);
        //yield return image;
        //image.LoadImageIntoTexture(texture);
        int txSize = texture.width;
        pack.Thumb = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        Debug.Log("Loaded image size: " + texture.width + "x" + texture.height);
    }

}

public class Content
{
    [JsonProperty("name")]
    public string Name;

    [JsonProperty("url")]
    public string Url;

    [JsonProperty("uid")]
    public string Uid;

    public override string ToString()
    {
        return "\nUid = " + Uid
            + "\nName = " + Name
            + "\nUrl = " + Url;
    }
    public bool IsVideoFile { get { return Name.Contains(".mp4"); } }

}

public class Root
{
    [JsonProperty("member")]
    public Member Member;
}

public class SessionMember
{
    [JsonProperty("email")]
    public string Email;

    [JsonProperty("first_name")]
    public string FirstName;

    [JsonProperty("last_name")]
    public string LastName;

    [JsonProperty("role")]
    public string Role;

    [JsonProperty("uid")]
    public string Uid;

    public string DisplayName { get { return FirstName + " " + LastName; } }

    public override string ToString()
    {
        return "\nUid = " + Uid
            + "\nEmail = " + Email
        + "\nName = " + DisplayName
            + "\nRole = " + Role;
    }
}

public class Session
{
    [JsonProperty("name")]
    public string Name;

    [JsonProperty("time")]
    public string Time;

    [JsonProperty("time_stamp")]
    public int TimeStamp;

    [JsonProperty("uid")]
    public string Uid;

    [JsonProperty("content")]
    public List<Content> Content;

    [JsonProperty("members")]
    public List<SessionMember> Members;

    public SessionDownloadManager sessionDownloadManager;

    public override string ToString()
    {
        string s = "Name = " + Name
            + "\ntime = " + Time
            + "\nTimeStamp = " + TimeStamp.ToString()
            + "\nUid = " + Uid
            + "\n\ncontent";

        foreach (var item in Content)
        {
            s += item.ToString();
        }
        s += "\n\nmembers";
        foreach (var item in Members)
        {
            s += item.ToString();
        }
        return s;
    }

}

public class Member
{
    [JsonProperty("contact")]
    public string Contact;

    [JsonProperty("email")]
    public string Email;

    [JsonProperty("first_name")]
    public string FirstName;

    [JsonProperty("last_name")]
    public string LastName;

    [JsonProperty("member_code")]
    public int MemberCode;

    [JsonProperty("sessions")]
    public List<Session> Sessions;

    [JsonProperty("uid")]
    public string Uid;

    public string DisplayName {get{ return FirstName + " " + LastName; }}

    public override string ToString()
    {
        string s = "~~~~~~~~~~~~~~~~~~"
            + "\nUid = " + Uid
            +"\nDisplay Name = " + DisplayName
            + "\nEmail = " + Email
            + "\nMemberCode = " + MemberCode
            + "\ncontact = " + Contact
            + "\nfirst_name = " + FirstName
            + "\nlast_name = " + LastName
            + "\n\nSession Code List "
            ;
        if (Sessions != null)
        {
            int index = 1;
            foreach (var item in Sessions)
            {
                s += "\n" + index++ + " = " + item;
            }
        }
        s += "\n~~~~~~~~~~~~~~~~~~";
        return s;
    }
}


