﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SplashController : MonoBehaviour
{
    public GameObject mainCanvas;
    CanvasGroup canvasGroup;
    CanvasGroup mainPanelCanvasGroup;

    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        mainPanelCanvasGroup = mainCanvas.GetComponent<CanvasGroup>();
        mainCanvas.SetActive(false);
        fadeVal = 0;
        canvasGroup.alpha = fadeVal;
        mainPanelCanvasGroup.alpha = fadeVal;
        StartCoroutine(FadeInOut());
    }
    float fadeVal = 1;
    float fadeSpeed = .5f;
    private IEnumerator FadeInOut()
    {
        yield return new WaitForSeconds(1);
        while (fadeVal < 1)
        {
            yield return new WaitForEndOfFrame();
            fadeVal += Time.deltaTime * fadeSpeed;
            canvasGroup.alpha = fadeVal;
        }
        fadeVal = 1;
        canvasGroup.alpha = fadeVal;
        yield return new WaitForSeconds(1);
        mainCanvas.SetActive(true);
        while (fadeVal > 0)
        {
            yield return new WaitForEndOfFrame();
            fadeVal -= Time.deltaTime * fadeSpeed;
            canvasGroup.alpha = fadeVal;
            mainPanelCanvasGroup.alpha = 1-fadeVal;
        }
        fadeVal = 0;
        canvasGroup.alpha = fadeVal;
        mainPanelCanvasGroup.alpha = 1 - fadeVal;

        this.gameObject.SetActive(false);
    }
}
