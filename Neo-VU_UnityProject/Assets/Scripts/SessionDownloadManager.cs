﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.IO;

public class SessionDownloadManager : MonoBehaviour
{
    const string initialString = "Media Status :";

    public TextMeshProUGUI MediaStatusText;
    int totalCount = 0;
    int downloadedCount = 0;
    MediaBrowser browser;
    List<MediaPack> videoPacks;
    public bool IsMediaDownloaded { get { return totalCount == downloadedCount; } }
    public System.Action OnMediaDownload;
    public void Initialize(Session session)
    {
        //StartCoroutine(startSessionMediaSync(session));
    }
    public IEnumerator startSessionMediaSync(Session session)
    {
        totalCount = session.Content.Count;
        browser = FindObjectOfType<MediaBrowser>();
        videoPacks = browser.videoPacks;
        downloadedCount = 0;
        foreach (Content item in session.Content)
        {
            bool isTherePackForThis = (videoPacks.Where(x => x.Uid == item.Uid).Count() > 0);
            bool isPackFileDownloaded = false;
            if(isTherePackForThis)
                isPackFileDownloaded = videoPacks.Where(x => x.Uid == item.Uid).FirstOrDefault().Downloaded;
            downloadedCount += isTherePackForThis ? isPackFileDownloaded ? 1 : 0 : 0;
        }
        MediaStatusText.text = initialString + " " + downloadedCount + "/" + totalCount;
        float missingCount = totalCount - downloadedCount;
        float downloadProgress = 0;
        float[] indualProgress = new float[totalCount];
        if (missingCount != 0)
        {
            foreach (Content item in session.Content)
            {
                bool isTherePackForThis = (videoPacks.Where(x => x.Uid == item.Uid).Count() > 0);
                bool isPackFileDownloaded = false;
                if (isTherePackForThis)
                    isPackFileDownloaded = videoPacks.Where(x => x.Uid == item.Uid).FirstOrDefault().Downloaded;

                bool onContentDownloadComplete = false;

                if (!(isTherePackForThis && isPackFileDownloaded))
                {
                    browser.SetUpVideoPack(item,
                    (float p , int packIndex) =>
                    {
                        int contentIndex = session.Content.IndexOf(item);
                        downloadProgress += p - indualProgress[contentIndex];
                        indualProgress[contentIndex] = p;
                        //string ProgressString = " Downloading (" + (int)((downloadProgress / missingCount) * 100) + "%)";
                        string ProgressString = " Downloading ("+ (contentIndex + 1) + "/" + totalCount + ") (" + (int)(p * 100) + "%)";

                        MediaStatusText.text = initialString + " " + downloadedCount + "/" + totalCount + ProgressString;
                        //Debug.Log(initialString + " " + downloadedCount + "/" + totalCount + ProgressString);
                    },
                    onDone: () =>
                     {
                         downloadedCount++;
                         MediaStatusText.text = initialString + " " + downloadedCount + "/" + totalCount;
                         Debug.Log(initialString + " " + downloadedCount + "/" + totalCount);
                         onContentDownloadComplete = true;
                         if (IsMediaDownloaded)
                         {
                             OnMediaDownload?.Invoke();
                         }

                     });
                    while (!onContentDownloadComplete)
                    {
                        yield return new WaitForEndOfFrame();
                    }

                }
                //else 
                //{

                //}
            }
        }
        else 
        {
            OnMediaDownload?.Invoke();
        }
        //while (totalCount - downloadedCount != 0)
        //{ 
            yield return new WaitForEndOfFrame();
        //}
    }

}
