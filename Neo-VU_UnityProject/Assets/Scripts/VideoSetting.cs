﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoSetting : MonoBehaviour
{
    public Transform Dome;
    public Slider ZoomSlider;
    public Transform user;
    void Start()
    {
        
    }

    public void OnZoomVelueChange()
    {
        Dome.position = user.forward * -ZoomSlider.value;
    }

}
