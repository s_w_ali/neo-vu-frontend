﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Image))]
public class TogleImageButton : MonoBehaviour
{
    public Sprite OnImage;
    public Sprite OffImage;
    public bool DefaultState;

    Image ImageComponent;
    private bool state;

    private void Awake()
    {
        state = DefaultState;
        ImageComponent = GetComponent<Image>();
    }

    public void TogleState()
    {
        state = !state;

        if (state)
            ImageComponent.sprite = OnImage;
        else
            ImageComponent.sprite = OffImage;
    }


}
