﻿using ExitGames.Client.Photon;
using Photon.Realtime;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Pun.Demo.Asteroids;
using TMPro;
using System.Collections;

public class LobbyMenuController : MonoBehaviourPunCallbacks , ILobbyCallbacks
{
    [Header("Login Panel")]
    public GameObject LoginPanel;

    public Button LoginButton;
    public InputField PlayerNameInput;
    public TextMeshProUGUI LoginStatusText;
    public TextMeshProUGUI UserNameText;

    [Header("Selection Panel")]
    public GameObject SelectionPanel;

    [Header("Create Room Panel")]
    public GameObject CreateRoomPanel;

    public InputField RoomNameInputField;
    public InputField MaxPlayersInputField;

    [Header("Media Selection")]
    public GameObject MediaSelectionPanel;
    public Button GoToSessionListButton;

    [Header("Room List Panel")]
    public GameObject RoomListPanel;

    public GameObject RoomListContent;
    public GameObject RoomListEntryPrefab;

    [Header("Inside Room Panel")]
    public GameObject InsideRoomPanel;

    public Button StartGameButton;
    public GameObject UserListEntryPrefab;
    public GameObject UserListParent;
    public SessionInfoPanel SessionInfoPanel;

    [Header("Session list Panel")]
    public GameObject SessionListPanel;
    public GameObject SessionListParent;
    public GameObject SessionEntryPrefab;

    [Header("Media Browser Panel")]
    public GameObject MediaBrowserPanel;
    public GameObject MediaBrowserContentParent;
    public GameObject MediaBrowserButtonPrefab;

    public TMPro.TextMeshProUGUI DebugText;

    private Dictionary<string, RoomInfo> cachedRoomList;
    private Dictionary<string, GameObject> roomListEntries;


    private int playerCount;
    private Dictionary<int, GameObject> playerListEntries
    {
        get
        {
            return Manager.Instance.playerListEntries;
        }
        set
        {
            Manager.Instance.playerListEntries = value;
        }
    }

    private TouchScreenKeyboard overlayKeyboard;
    public static string inputText = "";

    WebComBase webComBase { get { return mediaBrowser.webComBase; } }

    #region UNITY

    public void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        cachedRoomList = new Dictionary<string, RoomInfo>();
        roomListEntries = new Dictionary<string, GameObject>();

        //PlayerNameInput.text = "Player " + Random.Range(1000, 10000);
        if (PlayerPrefs.HasKey("UserName"))
            PlayerNameInput.text = PlayerPrefs.GetString("UserName");

        LoginStatusText.text = "";
        UserNameText.text = "";

        if (PhotonNetwork.IsConnected)
            SetupSessionList();
        else
            SetActivePanel(LoginPanel.name);

        _mediaBrowser = FindObjectOfType<MediaBrowser>();

    }

    #endregion

    #region PUN CALLBACKS

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetupSessionList();
    }
    MediaBrowser _mediaBrowser;
    MediaBrowser mediaBrowser { get { return _mediaBrowser != null ? _mediaBrowser : FindObjectOfType<MediaBrowser>(); } }
    List<SessionInfo> sessionInfos;
    void SetupSessionList()
    {
        if (sessionInfos != null)
            sessionInfos.Clear();
        sessionInfos = new List<SessionInfo>();

        foreach (Transform item in SessionListParent.transform)
        {
            Destroy(item.gameObject);
        }
        foreach (Session session in Manager.Instance.member.Sessions)
        {
            GameObject sessionEntery = Instantiate(SessionEntryPrefab, SessionListParent.transform);

            session.sessionDownloadManager = sessionEntery.GetComponent<SessionInfo>().Initialize(session, Manager.Instance.member,cachedRoomList.ContainsKey(session.Name)).sessionDownloadManager;
            sessionInfos.Add(sessionEntery.GetComponent<SessionInfo>());
        }
        mediaBrowser.DownloadSesstionMedia(0);
        this.SetActivePanel(SessionListPanel.name);
        UpdateSessionInfoList();
        setLocalPlayerUID();
    }
    void setLocalPlayerUID()
    {
        ExitGames.Client.Photon.Hashtable setValue = new ExitGames.Client.Photon.Hashtable();
        if (!PhotonNetwork.LocalPlayer.CustomProperties.ContainsKey(Manager.PlayerUidString))
        {
            setValue.Add(Manager.PlayerUidString, Manager.Instance.member.Uid);
            PhotonNetwork.LocalPlayer.SetCustomProperties(setValue);
        }

        Debug.Log("debug ++CustomProperties++");
        foreach (var item in PhotonNetwork.LocalPlayer.CustomProperties)
        {
            Debug.Log("++ key =" + item.Key.ToString() + " || Val = " + item.Value.ToString());
        }
    }
    void UpdateSessionInfoList()
    {
        foreach (SessionInfo sInfo in sessionInfos)
        {
            sInfo.RoomListUpdated(cachedRoomList.ContainsKey(sInfo.session.Name));
        }
    }
    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        LoginButton.interactable = true;
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        //ClearRoomListView();
        Debug.Log("Room list updated");
        UpdateCachedRoomList(roomList);
        //SetupSessionList();
        //UpdateRoomListView();
    }

    public override void OnJoinedLobby()
    {
        // whenever this joins a new lobby, clear any previous room lists
        cachedRoomList.Clear();
        ClearRoomListView();
    }

    // note: when a client joins / creates a room, OnLeftLobby does not get called, even if the client was in a lobby before
    public override void OnLeftLobby()
    {
        cachedRoomList.Clear();
        ClearRoomListView();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        //SetActivePanel(SelectionPanel.name);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        SetActivePanel(SessionListPanel.name);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        string roomName = "Room " + Random.Range(1000, 10000);

        RoomOptions options = new RoomOptions { MaxPlayers = 8 };

        PhotonNetwork.CreateRoom(roomName, options, null);
    }

    public override void OnJoinedRoom()
    {
        // joining (or entering) a room invalidates any cached lobby room list (even if LeaveLobby was not called due to just joining a room)
        cachedRoomList.Clear();

        SetActivePanel(InsideRoomPanel.name);

        if (playerListEntries == null)
        {
            playerListEntries = new Dictionary<int, GameObject>();
        }
        foreach (Transform item in UserListParent.transform)
        {
            Destroy(item.gameObject);
        }
        List<Color> PlayersColorList = new List<Color>();
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            GameObject entry = Instantiate(UserListEntryPrefab);
            entry.transform.SetParent(UserListParent.transform);
            entry.transform.localScale = Vector3.one;
            entry.transform.localPosition = Vector3.zero;
            
            entry.GetComponent<UserListEntry>().Initialize(p.ActorNumber, p.NickName);
         
            //object isPlayerReady;
            //if (p.CustomProperties.TryGetValue(AsteroidsGame.PLAYER_READY, out isPlayerReady))
            //{
            //    entry.GetComponent<UserListEntry>().SetPlayerReady((bool)isPlayerReady);
            //}j
            if(!playerListEntries.ContainsKey(p.ActorNumber))
                playerListEntries.Add(p.ActorNumber, entry);
        }
        //if(PhotonNetwork.IsMasterClient)
        //    PhotonNetwork.LocalPlayer.CustomProperties.Add(PlayerNumbering.RoomPlayerIndexedProp, PhotonNetwork.PlayerList.Length-1);
        SessionInfoPanel.Initialize(Manager.Instance.SelectedSession);
        StartGameButton.gameObject.SetActive(CheckPlayersReady());

    }

    public override void OnLeftRoom()
    {
        SetActivePanel(SessionListPanel.name);
        PhotonNetwork.LocalPlayer.CustomProperties.Clear();
        foreach (GameObject entry in playerListEntries.Values)
        {
            if (entry != null) ;
            Destroy(entry.gameObject);
        }

        playerListEntries.Clear();
        playerListEntries = null;
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        GameObject entry = Instantiate(UserListEntryPrefab);
        entry.transform.SetParent(UserListParent.transform);
        entry.transform.localScale = Vector3.one;
        entry.transform.localPosition = Vector3.zero;
        entry.GetComponent<UserListEntry>().Initialize(newPlayer.ActorNumber, newPlayer.NickName);
        playerListEntries.Add(newPlayer.ActorNumber, entry);
        StartGameButton.gameObject.SetActive(CheckPlayersReady());
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if (playerListEntries.ContainsKey(otherPlayer.ActorNumber))
        {
            Destroy(playerListEntries[otherPlayer.ActorNumber].gameObject);
            playerListEntries.Remove(otherPlayer.ActorNumber);
        }
        StartGameButton.gameObject.SetActive(CheckPlayerIsPresenter(Manager.Instance.SelectedSession));

        // if the player left is presenter leave lobby.
        
        if(Manager.Instance.SelectedSession.Members.Where(x => x.Uid == Manager.GetUserID(otherPlayer)).FirstOrDefault().Role == Manager.PresenterRoleString)
        {
            OnBackButtonClicked();
        }

    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            StartGameButton.gameObject.SetActive(CheckPlayerIsPresenter(Manager.Instance.SelectedSession));
        }
    }

    public override void OnPlayerPropertiesUpdate(Player targetPlayer, ExitGames.Client.Photon.Hashtable changedProps)
    {
        if (playerListEntries == null)
        {
            playerListEntries = new Dictionary<int, GameObject>();
        }

        GameObject entry;
        if (playerListEntries.TryGetValue(targetPlayer.ActorNumber, out entry))
        {
            //object isPlayerReady;
            //if (changedProps.TryGetValue(AsteroidsGame.PLAYER_READY, out isPlayerReady))
            //{
            //    entry.GetComponent<UserListEntry>().SetPlayerReady((bool)isPlayerReady);
            //}
        }

        StartGameButton.gameObject.SetActive(CheckPlayerIsPresenter(Manager.Instance.SelectedSession));
    }

    #endregion

    #region UI CALLBACKS

    public void OnBackButtonClicked()
    {
        if (PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }
        FindObjectOfType<MediaBrowser>().BackButton();
        SetActivePanel(SessionListPanel.name);
    }

    public void OnSignOutButtonClicked()
    {
        PhotonNetwork.Disconnect();
        //webComBase.StopAllCurrentDownloads();
        UserNameText.text = "";
        webComBase.AbortDownload();
        webComBase.StopAllCoroutines();
        SetActivePanel(LoginPanel.name);
    }

    public void OnCreateRoomStartedButtonClicked()
    {
        SetActivePanel(MediaSelectionPanel.name);
        FindObjectOfType<MediaBrowser>().OnBrowseropen();
    }


    public void OnCreateOrJoinSessiondButtonClicked(Session s,bool IsPresenter)
    {
        Manager.Instance.SelectedSession = s;
        if (IsPresenter)
        {
            OnCreateRoomStartedButtonClicked();
        }
        else 
        {

            if (PhotonNetwork.InLobby)
            {
                PhotonNetwork.LeaveLobby();
            }

            PhotonNetwork.JoinRoom(Manager.Instance.SelectedSession.Name);
        }
    }

    public MediaPack SelectedVideoPack;
    public void OnCreateRoomButtonClicked()
    {
        string roomName = RoomNameInputField.text;
        roomName = (roomName.Equals(string.Empty)) ? "Session " + Random.Range(1000, 10000) : roomName;

        byte maxPlayers;
        //byte.TryParse(MaxPlayersInputField.text, out maxPlayers);
        //maxPlayers = (byte)Mathf.Clamp(maxPlayers, 2, 20);
        maxPlayers = (byte)20;
        ExitGames.Client.Photon.Hashtable setValue = new ExitGames.Client.Photon.Hashtable();
        setValue.Add("SelectedVideoPack", SelectedVideoPack.Uid);
        setValue.Add(Manager.PlayerUidString, Manager.Instance.member.Uid);

        RoomOptions options = new RoomOptions { MaxPlayers = maxPlayers, PlayerTtl = 10000, CustomRoomProperties = setValue, IsVisible = true , PublishUserId = true};

        PhotonNetwork.CreateRoom(Manager.Instance.SelectedSession.Name, options);
    }

    public void OnLeaveGameButtonClicked()
    {
        FindObjectOfType<MediaBrowser>().BackButton();
        PhotonNetwork.LeaveRoom();
    }
    
    public void OnLoginButtonClicked()
    {
        mediaBrowser.UpdateMediaPackListFromDisk();

        string playerName = PlayerNameInput.text;
        webComBase.GetMemberinfo(playerName,
            (object responceObject) =>
            {
                if (responceObject == null)
                    return;

                Manager.Instance.member = (Member)responceObject;
                PlayerPrefs.SetString("UserName", playerName);
                ConnectToPhotonServer(Manager.Instance.member.DisplayName);
                UserNameText.text = Manager.Instance.member.DisplayName;

                Debug.Log(Manager.Instance.member);
                DebugText.text= Manager.Instance.member.ToString();
                //if (member.SessionCodes.Length > 0)
                //{
                //    sessions = new List<Session>();
                //    foreach (var item in member.SessionCodes)
                //    {
                //        webComBase.GetSessioninfo(item,
                //            (object responceSessionObject) =>
                //            {
                //                if (responceSessionObject == null)
                //                    return;

                //                sessions.Add((Session)responceSessionObject);

                //                Debug.Log(sessions);
                //            }
                //            );
                //    }
                //}
            },
            (string error)=>
            {
                LoginStatusText.text = "Code invalid! please enter correct code and try again.";
                UserNameText.text = "";
                Debug.LogError("Code Error." + error);
                DebugText.text = ("Code Error." + error);
            }
            );
        if (!playerName.Equals(""))
        {
            LoginStatusText.text = "";
            //LoginButton.interactable = false;
        }
        else
        {
            LoginStatusText.text = "Please enter code to continue,";
            Debug.LogError("Code is empty.");
        }
    }
    void ConnectToPhotonServer(string playerName)
    {
        PhotonNetwork.LocalPlayer.NickName = playerName;
        ExitGames.Client.Photon.Hashtable setValue = new ExitGames.Client.Photon.Hashtable();
       
        setValue.Add(Manager.SelectedVideoPackString, SelectedVideoPack.Uid);
        setValue.Add(Manager.PlayerUidString, Manager.Instance.member.Uid);
        //setValue.Add(Manager.SelectedSessiontring, Manager.Instance.SelectedSession.Uid);

        PhotonNetwork.LocalPlayer.SetCustomProperties(setValue);
        
        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "eu";
        PhotonNetwork.PhotonServerSettings.AppSettings.UseNameServer = true;
        PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = "758292a4-97c7-4af0-b4f9-d2554c711cae";
        PhotonNetwork.ConnectUsingSettings();
    }
    public void OnRoomListButtonClicked()
    {
        if (!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }

        SetActivePanel(RoomListPanel.name);
    }

    public void OnStartGameButtonClicked()
    {
        PhotonNetwork.CurrentRoom.IsOpen = true;
        PhotonNetwork.CurrentRoom.IsVisible = true;

        PhotonNetwork.LoadLevel("VideoScene");
    }

    #endregion
    public bool CheckPlayerIsPresenter(Session session)
    {
        Member m = Manager.Instance.member;
        SessionMember sm = session.Members.Where(x => x.Uid == m.Uid).FirstOrDefault();

        return sm.Role == Manager.PresenterRoleString;
    }
    private bool CheckPlayersReady()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return false;
        }
        //foreach (Player p in PhotonNetwork.PlayerList)
        //{
        //    object isPlayerReady;
        //    if (p.CustomProperties.TryGetValue(AsteroidsGame.PLAYER_READY, out isPlayerReady))
        //    {
        //        if (!(bool)isPlayerReady)
        //        {
        //            return false;
        //        }
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        return true;
    }

    private void ClearRoomListView()
    {
        foreach (GameObject entry in roomListEntries.Values)
        {
            Destroy(entry.gameObject);
        }

        roomListEntries.Clear();
    }

    public void LocalPlayerPropertiesUpdated()
    {
        StartGameButton.gameObject.SetActive(CheckPlayersReady());
    }

    //************* Offline media browser ********
    public void MediaBrowserBackButton()
    {
        SetActivePanel(SessionListPanel.name);
    }
    public void OpenMediaBrowser()
    {
        SetActivePanel(MediaBrowserPanel.name);
        SetupAllVideoButtons();
    }

    private void SetupAllVideoButtons()
    {
        List<MediaPack> MemeberMediaPackList = new List<MediaPack>();
        foreach (Transform item in MediaBrowserContentParent.transform)
        {
            Destroy(item.gameObject);
        }
        foreach (Session session in Manager.Instance.member.Sessions)
        {
            foreach (Content content in session.Content)
            {
                if(!MemeberMediaPackList.Any(x => x.Uid == content.Uid))
                    MemeberMediaPackList.AddRange(mediaBrowser.videoPacks.Where(x => x.Uid == content.Uid));             
            }
        }

        foreach (MediaPack mediaPack in MemeberMediaPackList)
        {
            GameObject button = InstantiateButton(mediaPack);
        }

    }
    GameObject InstantiateButton(MediaPack VideoFile)
    {
        GameObject newButton = Instantiate(MediaBrowserButtonPrefab, MediaBrowserContentParent.transform);
        newButton.transform.Find("Name").GetComponent<TextMeshProUGUI>().text = VideoFile.Name;
        MediaPack imagePack = mediaBrowser.videoPacks.Where(x => x.FilePath.ToUpper().Contains((VideoFile.Name + ".jpg").ToUpper())).FirstOrDefault();
        if (imagePack != null)
            newButton.transform.Find("Image").GetComponent<Image>().sprite = imagePack.Thumb;
        newButton.GetComponent<VideoButtonInfo>().MyPack = VideoFile;
        newButton.GetComponent<VideoButtonInfo>().IsOffline = true;
        return newButton;
    }
    //************* ********************* ********

    public void SetActivePanel(string activePanel)
    {
        LoginPanel.SetActive(activePanel.Equals(LoginPanel.name));
        SelectionPanel.SetActive(activePanel.Equals(SelectionPanel.name));
        CreateRoomPanel.SetActive(activePanel.Equals(CreateRoomPanel.name));
        MediaSelectionPanel.SetActive(activePanel.Equals(MediaSelectionPanel.name));
        RoomListPanel.SetActive(activePanel.Equals(RoomListPanel.name));
        InsideRoomPanel.SetActive(activePanel.Equals(InsideRoomPanel.name));
        SessionListPanel.SetActive(activePanel.Equals(SessionListPanel.name));
        MediaBrowserPanel.SetActive(activePanel.Equals(MediaBrowserPanel.name));
    }

    private void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        foreach (RoomInfo info in roomList)
        {
            // Remove room from cached room list if it got closed, became invisible or was marked as removed
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList.Remove(info.Name);
                }

                continue;
            }

            // Update cached room info
            if (cachedRoomList.ContainsKey(info.Name))
            {
                cachedRoomList[info.Name] = info;
            }
            // Add new room info to cache
            else
            {
                cachedRoomList.Add(info.Name, info);
            }
        }
        UpdateSessionInfoList();
    }

    private void UpdateRoomListView()
    {
        foreach (RoomInfo info in cachedRoomList.Values)
        {
            GameObject entry = Instantiate(RoomListEntryPrefab);
            entry.transform.SetParent(RoomListContent.transform);
            entry.transform.localScale = Vector3.one;
            entry.transform.localPosition = Vector3.zero;
            entry.GetComponent<RoomListEntry>().Initialize(info.Name, (byte)info.PlayerCount, info.MaxPlayers);

            roomListEntries.Add(info.Name, entry);
        }
    }

}
