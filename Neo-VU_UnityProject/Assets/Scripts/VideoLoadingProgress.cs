﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class VideoLoadingProgress : MonoBehaviour
{
    public Image LoadingProgress;
    public TextMeshProUGUI LoadingProgresstext;
    public GameObject loadingGroup;
    public Button Button;

    public float Progress { 
        set 
        {
            if (LoadingProgress != null)
            {
                LoadingProgress.fillAmount = value;
                LoadingProgresstext.text = (int)(value * 100) + "%";
                bool condetion = (value > 0 && value < 1f);
                Button.interactable = !condetion;
                loadingGroup.SetActive(condetion);
            }
        } 
    }

    public void PregressCallback(float val)
    {
        Progress = val;
    }

}
