﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoButtonInfo : MonoBehaviour
{
    public MediaPack MyPack;
    bool _offline = false;
    public bool IsOffline 
    { 
        set 
        { 
            _offline = value; 
        }
        get 
        {
            return _offline;
        }
    }
    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    void OnClick()
    {
        if (IsOffline)
        {
            // Load Video play scene for this pack.
        }
        else
        { 
            FindObjectOfType<MediaBrowser>().ButtonClicked(MyPack);
        }
    }

}
