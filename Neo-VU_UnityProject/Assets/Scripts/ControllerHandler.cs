﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerHandler : MonoBehaviour
{
    public GameObject Controller;
    public OVRInput.Controller m_controller;

    private bool m_prevControllerConnected = false;
    private bool m_prevControllerConnectedCached = false;
    void Start()
    {
        Controller.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        bool controllerConnected = OVRInput.IsControllerConnected(m_controller);

        //if ((controllerConnected != m_prevControllerConnected) || !m_prevControllerConnectedCached)
        if (controllerConnected)
        {
            Controller.SetActive(true);

            //m_prevControllerConnected = controllerConnected;
            //m_prevControllerConnectedCached = true;
        }
        else
        {
            Controller.SetActive(false);
        }
    }
}
