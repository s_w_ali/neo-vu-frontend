﻿using System.Collections;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MediaListEntery : MonoBehaviour
{
    public Text MediaNameText;
    public Image Image;
    public Image Selected;
    MediaBrowser mediaBrowser { get { return FindObjectOfType<MediaBrowser>(); } }

    public void Initialize(Content content , bool IsSelected = false)
    {
        MediaNameText.text = content.Name;
        MediaPack iconPack = mediaBrowser.videoPacks.Where(x => x.FilePath.ToUpper().Contains((Path.GetFileNameWithoutExtension(content.Name) + ".jpg").ToUpper())).FirstOrDefault(); ;
        if (iconPack != null)
        {
            Image.sprite = iconPack.Thumb;
        }
        Selected.gameObject.SetActive(IsSelected);
    }
}
