﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class ParticipantsListManager : MonoBehaviourPunCallbacks
{
    public GameObject participantParent;
    public GameObject ParticipantEnteryPrefab;

    VideoController _videoController;
    VideoController videoController 
    { 
        get 
        { 
            if(_videoController == null)
                _videoController = FindObjectOfType<VideoController>();
            return _videoController;
        } 
    }

    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        foreach (var item in PhotonNetwork.PlayerList)
        {
            GameObject entery = Instantiate(ParticipantEnteryPrefab, participantParent.transform);
            entery.GetComponent<ParticipantEntery>().Initialize(item);
        }
    }
    string GetUserId (Player thisPlayer)
    { 
        return thisPlayer.CustomProperties[Manager.PlayerUidString].ToString();
    } 
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        GameObject entery = Instantiate(ParticipantEnteryPrefab, participantParent.transform);
        entery.GetComponent<ParticipantEntery>().Initialize(newPlayer);
        videoController.UpdateParticipantCount();
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        videoController.RemoveToPlayerGOList(GetUserId(otherPlayer));
        videoController.UpdateParticipantCount();
        if (Manager.IsUserPresenter(otherPlayer, Manager.Instance.SelectedSession))
        {
            videoController.Menu();
        }
    }


}
