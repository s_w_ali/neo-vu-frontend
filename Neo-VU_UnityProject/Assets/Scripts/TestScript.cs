﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
public class TestScript : MonoBehaviour
{
    public TextMeshProUGUI DebugText;

    public void buttonClicked()
    {
        if(DebugText != null)
        DebugText.text = "Button Clicked";
        SceneManager.LoadScene(3);
        //StartCoroutine(ResetText());
    }

    IEnumerator ResetText()
    {
        yield return new WaitForSeconds(15);
        DebugText.text = "...";
    }

}
