﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class SessionInfo : MonoBehaviour
{
    public TextMeshProUGUI Name;
    public TextMeshProUGUI Time;
    public Button SessionCreateJoinButton;

    private Session _session;
    public Session session { get { return _session; } }
    private Member CurrentMember;
    private SessionDownloadManager downloadManager;

    private bool IsPresenterForThisSession;
    public Session Initialize(Session s, Member m, bool roomExists)
    {
        _session = s;
        CurrentMember = m;
        Name.text = s.Name;
        Time.text = System.DateTime.Parse(s.Time).ToString();
        downloadManager = GetComponent<SessionDownloadManager>();
        SessionMember sm = s.Members.Where(x => x.Uid == m.Uid).FirstOrDefault();

        IsPresenterForThisSession = sm.Role == "presenter";

        if (IsPresenterForThisSession)
        {
            SessionCreateJoinButton.transform.GetComponentInChildren<Text>().text = "Start Session";
        }
        else
        {
            SessionCreateJoinButton.transform.GetComponentInChildren<Text>().text = "Join Session";
            SessionCreateJoinButton.interactable = roomExists && downloadManager.IsMediaDownloaded;
            if (!downloadManager.IsMediaDownloaded)
            {
                downloadManager.OnMediaDownload += () => { SessionCreateJoinButton.interactable = roomExists; };
            }
        }
        s.sessionDownloadManager = downloadManager;
        //downloadManager.Initialize(_session); // downloading on separate thread on the LobbymenuController
        return s;
    }
    public void RoomListUpdated(bool roomExists)
    {
        if (downloadManager.IsMediaDownloaded && !IsPresenterForThisSession)
        {
            SessionCreateJoinButton.interactable = roomExists;
        }
        
    }

    public void OnClick()
    {
        FindObjectOfType<LobbyMenuController>().OnCreateOrJoinSessiondButtonClicked(_session, IsPresenterForThisSession);
    }
}
