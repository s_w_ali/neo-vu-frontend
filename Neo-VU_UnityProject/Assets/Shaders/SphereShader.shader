﻿Shader "Draw Sphere"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (0,0,0,1)
		_Radius("Radius",Float) = 1
		_Position("Position",Vector) = (0,0,0,1)
		_Rotation("Rotation", float) = -90
	}
	Subshader
	{
		Tags { "Queue" = "Background" "RenderType" = "Background"}

		Cull Off ZWrite On
		Pass
		{
			Cull Off
			CGPROGRAM
			#pragma vertex VSMain
			#pragma fragment PSMain
			#pragma target 5.0
			#include "UnityCG.cginc"
			uniform float _Radius;
			uniform float4 _Color;
			uniform float4 _Position;
			float _Rotation;

			sampler2D _MainTex;
			float4 _MainTex_ST;

			struct v2f
			{
				float3 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float3 n    : TEXCOORD1;
			};
			v2f VSMain(uint id:SV_VertexID, float2 uv : TEXCOORD0)
			{
				v2f o;

				float rad = 64;
				float f = id;
				float v = f - 6.0 * floor(f / 6.0);
				f = (f - v) / 6.;
				float a = f - rad * floor(f / rad);
				f = (f - a) / rad;
				float b = f - 16.;
				a += (v - 2.0 * floor(v / 2.0));
				b += v == 2. || v >= 4. ? 1 : 0.0;
				a = a / rad * 6.28318;
				b = b / rad * 6.28318;
				float3 p = float3(cos(a) * cos(b), sin(b), sin(a) * cos(b));
				o.n = normalize(p);
				o.uv = p;
				p *= _Radius * 0.5;
				p += _Position.xyz;
				o.vertex = UnityObjectToClipPos(float4(p, 1.0));


				float s = sin((_Rotation / 180) * UNITY_PI);

				float c = cos((_Rotation / 180) * UNITY_PI);

				float2x2 rotationMatrix = float2x2(c, -s, s, c);

				rotationMatrix *= 0.5;

				rotationMatrix += 0.5;

				rotationMatrix = rotationMatrix * 2 - 1;

				o.uv.xz = mul(o.uv.xz, rotationMatrix);

				return o;
			}

			float4 PSMain(v2f i) : SV_Target
			{
				float3 dir = normalize(i.uv);

				float2 longlat = float2(atan2(dir.x , dir.z), acos(-dir.y));

				float2 uv = longlat / float2(1.0 * UNITY_PI, UNITY_PI);

				uv.x += 0.5;

				half4 tex = tex2D(_MainTex, TRANSFORM_TEX(uv, _MainTex));

				float4 color = float4(max(dot(normalize(_WorldSpaceLightPos0).xyz, i.n),0.0).xxx + UNITY_LIGHTMODEL_AMBIENT, 1.0) * _Color * tex;
				
				return tex;
			}
			ENDCG
		}

	}
}

