﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSphere : MonoBehaviour
{
    public Shader shader;
    public Material material;

    void Start()
    {
        if (material == null)
            material = new Material(shader);
    }

    void OnRenderObject()
    {
        material.SetPass(0);
        material.SetVector("_Position", new Vector4(transform.position.x, transform.position.y, transform.position.z, 1));
        Graphics.DrawProceduralNow(MeshTopology.Triangles, 12288*100, 1);
    }
}
